package com.accenture.devtools.test.cases.fileoperations;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.accenture.devtools.managers.FileManager;

public class CreateFileTests {

    private static String OUTPUT_PATH;
    private static String OUTPUT_PATH_TWO_LEVEL;

    @BeforeClass
    public static void setUp() {
	OUTPUT_PATH = "./test/com/accenture/devtools/test/outputs/";
	OUTPUT_PATH_TWO_LEVEL = "./test/com/accenture/devtools/test/outputs/level1/level2/";
    }

    @Test
    public void testCreateAndWrite() throws IOException {
	List<String> lines = Arrays.asList("first line", "second line");
	String filePath = OUTPUT_PATH.concat("output_ile.txt");
	FileManager.createAndWrite(filePath, lines);
    }

    @Test
    public void testCreate() throws IOException {
	String path = OUTPUT_PATH_TWO_LEVEL.concat("test_folder");
	FileManager.create(path);
    }

    @Test
    public void testWalkFileTree() throws IOException {

	Path projectFolder = Paths.get("./");
	Files.walkFileTree(projectFolder, new SimpleFileVisitor<Path>() {
	    @Override
	    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		System.out.println(file);
		return FileVisitResult.CONTINUE;
	    }

	});

    }

}
