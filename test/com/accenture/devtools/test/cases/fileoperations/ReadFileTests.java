package com.accenture.devtools.test.cases.fileoperations;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.accenture.devtools.managers.FileManager;

public class ReadFileTests {

    private static String testFile;
    private static List<String> testFileContents;

    @BeforeClass
    public static void setUp() {
	testFile = "./test/com/accenture/devtools/test/resources/test_file.txt";
	testFileContents = Arrays.asList("first line", "second line", "third line", "fourth line");
    }

    @Test
    public void testRead() throws IOException {
	// test content
	StringBuilder lines = new StringBuilder();
	testFileContents.forEach(record -> lines.append(record).append("\n"));
	String testData = lines.toString().trim();

	// read content
	String readData = FileManager.read(testFile);

	assertEquals(testData, readData);
    }

    @Test
    public void testGetClass() throws ClassNotFoundException {
	Class<?> clazz = Class.forName("com.accenture.devtools.test.resources.TestClass");
	for (Field field : clazz.getDeclaredFields()) {
	    System.out.println(field.getName());
	}
    }
}
