package com.accenture.devtools.test.cases.properties;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Properties;

import org.junit.BeforeClass;
import org.junit.Test;

import com.accenture.devtools.managers.PropertiesManager;

public class ReadPropertiesTest {

    private static String propertiesFileLocation;
    private static String propertiesFileName;

    @BeforeClass
    public static void setUp() {
	propertiesFileLocation = "./resources/config.properties";
	propertiesFileName = "config.properties";
    }

    @Test
    public void testReadPropertyWithReader() throws IOException {
	Properties config = new Properties();
	Reader reader = new FileReader(propertiesFileLocation);
	config.load(reader);
	System.out.println(config.toString());
    }

    @Test
    public void testReadPropertyWithInputStream() throws IOException {
	InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propertiesFileName);
	Properties properties = new Properties();
	properties.load(inputStream);
	System.out.println(properties);
    }

    @Test
    public void testReadPropertyFromManager() throws IOException {
	Properties properties = PropertiesManager.getProperties();
	System.out.println(properties);
    }
}
