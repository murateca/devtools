package com.accenture.devtools.test.cases.properties;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.junit.BeforeClass;
import org.junit.Test;

public class UpdatePropertiesTest {

    private static String propertiesFileLocation;

    @BeforeClass
    public static void setUp() {
	propertiesFileLocation = "./resources/config.properties";
    }

    @Test
    public void testUpdateProperty() throws IOException {
	FileInputStream in = new FileInputStream(propertiesFileLocation);
	Properties props = new Properties();
	props.load(in);
	in.close();

	FileOutputStream out = new FileOutputStream(propertiesFileLocation);
	props.setProperty("test", "updatedProperty1");
	props.store(out, "nothing important");
	out.close();

    }

}
