/**
 * 
 */
package com.accenture.devtools.test.resources;

import java.math.BigDecimal;

/**
 * @author murat.acet
 * 
 *         Test class
 *
 */
public class TestClass {
    private String field1;
    private Integer field2;
    private BigDecimal field3;
    private Float field4;
    private int field5;
    private char field6;

    /**
     * @return the field1
     */
    public String getField1() {
	return this.field1;
    }

    /**
     * @param field1
     *            the field1 to set
     */
    public void setField1(String field1) {
	this.field1 = field1;
    }

    /**
     * @return the field2
     */
    public Integer getField2() {
	return this.field2;
    }

    /**
     * @param field2
     *            the field2 to set
     */
    public void setField2(Integer field2) {
	this.field2 = field2;
    }

    /**
     * @return the field3
     */
    public BigDecimal getField3() {
	return this.field3;
    }

    /**
     * @param field3
     *            the field3 to set
     */
    public void setField3(BigDecimal field3) {
	this.field3 = field3;
    }

    /**
     * @return the field4
     */
    public Float getField4() {
	return this.field4;
    }

    /**
     * @param field4
     *            the field4 to set
     */
    public void setField4(Float field4) {
	this.field4 = field4;
    }

    /**
     * @return the field5
     */
    public int getField5() {
	return this.field5;
    }

    /**
     * @param field5
     *            the field5 to set
     */
    public void setField5(int field5) {
	this.field5 = field5;
    }

    /**
     * @return the field6
     */
    public char getField6() {
	return this.field6;
    }

    /**
     * @param field6
     *            the field6 to set
     */
    public void setField6(char field6) {
	this.field6 = field6;
    }

}
