package com.accenture.devtools;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

public final class Utils {

    static class Constants {
	private static final String iconPath = "/icons/devtools.png";

    }

    /**
     * returns devtools icon as swt.image
     * 
     * @param swtPage
     * @param display
     * @return Image
     */
    public static Image getIcon(Object swtPage, Display display) {
	return new Image(display, swtPage.getClass().getResourceAsStream(Constants.iconPath));
    }

    /**
     * shows information message for execution
     * 
     * @param event
     * @param title
     * @param message
     * @throws ExecutionException
     */
    public static void showInfoMessage(ExecutionEvent event, String title, String message) throws ExecutionException {
	IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
	MessageDialog.openInformation(window.getShell(), title, message);
    }

    /**
     * creates new unresizeable shell
     * 
     * if you want to design swt page with windows builder <b>DO NOT</b> generate
     * shell with this code since this method cannot be called from designer it will
     * cause a crash in design page. Also getting icon will cause an error on
     * standalone. Use this method only after everything has ready to go. For
     * standalone run or in design phase just copy and use first line of this method
     * 
     * @return shell
     */
    public static Shell createNewShell(Object swtPage, Display display, int width, int height, String title) {
	Shell shell = new Shell(SWT.SHELL_TRIM & (~SWT.RESIZE));
	// shell size
	shell.setSize(width, height);
	// title
	shell.setText(title);
	// setting the icon
	Image icon = getIcon(swtPage, display);
	shell.setImage(icon);
	// centering the shell
	Monitor primary = display.getPrimaryMonitor();
	Rectangle bounds = primary.getBounds();
	Rectangle rect = shell.getBounds();
	int x = bounds.x + (bounds.width - rect.width) / 2;
	int y = bounds.y + (bounds.height - rect.height) / 2;
	shell.setLocation(x, y);
	return shell;
    }

}
