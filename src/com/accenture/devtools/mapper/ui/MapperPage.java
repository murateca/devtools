/**
 * 
 */
package com.accenture.devtools.mapper.ui;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.Signature;
import org.eclipse.jdt.core.search.IJavaSearchConstants;
import org.eclipse.jdt.core.search.IJavaSearchScope;
import org.eclipse.jdt.core.search.SearchEngine;
import org.eclipse.jdt.core.search.SearchMatch;
import org.eclipse.jdt.core.search.SearchParticipant;
import org.eclipse.jdt.core.search.SearchPattern;
import org.eclipse.jdt.core.search.SearchRequestor;
import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.jface.bindings.keys.ParseException;
import org.eclipse.jface.fieldassist.ContentProposalAdapter;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.fieldassist.SimpleContentProposalProvider;
import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.accenture.devtools.Utils;

/**
 * @author murat.acet
 *
 */
public class MapperPage {
	private Logger logger;
	protected Shell shlDevToolsMapper;
	private Text txtSourceClass;
	private Label lblFindResult;
	private Button btnFind;

	// TODO move to utils
	/**
	 * @param match the search match
	 * @return the enclosing {@link IJavaElement}, or null iff none
	 */
	public static IJavaElement getEnclosingJavaElement(SearchMatch match) {
		Object element = match.getElement();
		if (element instanceof IJavaElement)
			return (IJavaElement) element;
		else
			return null;
	}

	/**
	 * launch the application.
	 * 
	 * @param args
	 * @wbp.parser.entryPoint
	 */
	public static void launch() {
		try {
			MapperPage window = new MapperPage();
			window.open();
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	/**
	 * open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents(display);
		shlDevToolsMapper.open();
		shlDevToolsMapper.layout();
		while (!shlDevToolsMapper.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * create contents of the window.
	 * 
	 * @param display
	 */
	protected void createContents(Display display) {
		//// shell
		shlDevToolsMapper = Utils.createNewShell(this, display, 450, 300, "Dev Tools Mapper");
		shlDevToolsMapper.setLayout(new GridLayout(2, false));
		//// ---------------- WIDGETS ----------------
		// label source class
		Label lblSourceClass = new Label(shlDevToolsMapper, SWT.NONE);
		lblSourceClass.setText("Source Class :");
		new Label(shlDevToolsMapper, SWT.NONE);
		// textbox source class
		txtSourceClass = new Text(shlDevToolsMapper, SWT.BORDER);
		txtSourceClass.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		// button find
		btnFind = new Button(shlDevToolsMapper, SWT.NONE);
		btnFind.setText("Find");
		// label find result
		lblFindResult = new Label(shlDevToolsMapper, SWT.NONE);
		new Label(shlDevToolsMapper, SWT.NONE);
		//// ---------------- EVENTS ----------------
		// enter key listener for txtSourceClass
		txtSourceClass.addTraverseListener(new TraverseListener() {
			@Override
			public void keyTraversed(TraverseEvent event) {
				// trigger selection event of find button
				btnFind.notifyListeners(SWT.Selection, new Event());
			}
		});
		// selection event for btnFind
		btnFind.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if(txtSourceClass.getText() == "") {
					changeLabelText(lblFindResult, "Please enter a class name");
					return;
				}
				SearchPattern pattern = SearchPattern.createPattern(txtSourceClass.getText(),
																	IJavaSearchConstants.TYPE,
																	IJavaSearchConstants.DECLARATIONS,
																	SearchPattern.R_PATTERN_MATCH | SearchPattern.R_CASE_SENSITIVE);
				IJavaSearchScope scope = SearchEngine.createWorkspaceScope();
//				MapperSearchRequestor requestor = new MapperSearchRequestor();
				SearchRequestor requestor = new SearchRequestor() {
					private boolean findFlag;
					
					/* (non-Javadoc)
					 * @see org.eclipse.jdt.core.search.SearchRequestor#beginReporting()
					 */
					@Override
					public void beginReporting() {
						findFlag = false;
					}
					
					/*
					 * (non-Javadoc)
					 * 
					 * @see
					 * org.eclipse.jdt.core.search.SearchRequestor#acceptSearchMatch(org.eclipse.jdt.core.search.SearchMatch)
					 */
					@Override
					public void acceptSearchMatch(SearchMatch match) throws CoreException {
						findFlag = true;
						IType element = (IType) getEnclosingJavaElement(match);
						changeLabelText(lblFindResult,
										element.getElementName() + " class is find under:\n" + match.getResource().getFullPath());
						String class1 = "CloneClass";					
						for (IField field : element.getFields()) {
							System.out.println(Signature.toString(field.getTypeSignature()) + " ----- " + field.getElementName());
						}
					}
					
					/* (non-Javadoc)
					 * @see org.eclipse.jdt.core.search.SearchRequestor#endReporting()
					 */
					@Override
					public void endReporting() {
						if(!findFlag) {
							changeLabelText(lblFindResult, "Class not found in workspace");
						}
					}					
				};
				// search
				SearchEngine searchEngine = new SearchEngine();
				try {
					searchEngine.search(pattern, 
										new SearchParticipant[] { SearchEngine.getDefaultSearchParticipant() },
										scope,
										requestor,
										null);
				} catch (CoreException cex) {
					logger.logp(Level.SEVERE, this.getClass().getName(), "btnFind.addListener", cex.getMessage());
				}
			}
		});
	}

	/**
	 * changes label text
	 * 
	 * @param label
	 * 
	 */
	private void changeLabelText(Label label, String text) {
		label.setText(text);
		label.getParent().layout();
	}

	/**
	 * adds propasal to textbox
	 * 
	 * @param text
	 */
	@SuppressWarnings("unused")
	private void addPropasal(Text text) {
		final ControlDecoration deco = new ControlDecoration(text, SWT.TOP | SWT.LEFT);
		Image image = FieldDecorationRegistry.getDefault().getFieldDecoration(FieldDecorationRegistry.DEC_INFORMATION).getImage();
		// set description and image
		deco.setDescriptionText("Use CTRL + SPACE to see possible values");
		deco.setImage(image);
		text.addModifyListener(e -> {
			Text source = (Text) e.getSource();
			if (!source.getText().isEmpty()) {
				deco.hide();
			} else {
				deco.show();
			}
		});
		// help the user with the possible inputs
		// "." and " " activate the content proposals
		char[] autoActivationCharacters = new char[] { '.', ' ' };
		KeyStroke keyStroke;
		try {
			keyStroke = KeyStroke.getInstance("Ctrl+Space");
			new ContentProposalAdapter(text, 
										new TextContentAdapter(),
										new SimpleContentProposalProvider(new String[] { "ProposalOne", "ProposalTwo", "ProposalThree" }),
										keyStroke,
										autoActivationCharacters);
		} catch (ParseException pex) {
			logger.logp(Level.SEVERE, this.getClass().getName(), "addPropasal", pex.getMessage());
		}
	}
}
