package com.accenture.devtools.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

import com.accenture.devtools.mapper.ui.MapperPage;

/**
 * handler for Mapper operation
 */
public class MapperHandler extends AbstractHandler {

    @SuppressWarnings("static-access")
    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
	new MapperPage().launch();
	return null;
    }
}
