package com.accenture.devtools.managers;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

public class FileManager {

    /**
     * creates file
     * 
     * @param path
     * @throws IOException
     */
    public static void create(String path) throws IOException {
	File file = new File(path);
	if (!file.getParentFile().exists()) {
	    file.mkdirs();
	}
	file.createNewFile();
    }

    /**
     * creates file and add lines to it
     * 
     * @param path
     * @param lines
     * @throws IOException
     */
    public static void createAndWrite(String path, List<String> lines) throws IOException {
	create(path);
	Path filePath = Paths.get(path);
	Files.write(filePath, lines, Charset.forName("UTF-8"));
    }

    /**
     * reads a file
     * 
     * @param path
     * @return lines as string
     * @throws IOException
     */
    public static String read(String path) throws IOException {
	Path filerPath = Paths.get(path);
	Stream<String> lines = Files.lines(filerPath);
	StringBuilder readData = new StringBuilder();
	lines.forEach(line -> readData.append(line).append("\n"));
	lines.close();
	return readData.toString().trim();
    }
}
