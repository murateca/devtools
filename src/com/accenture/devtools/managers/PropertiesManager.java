package com.accenture.devtools.managers;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesManager {

    private static String config = "config.properties";
    
    public static Properties getProperties() throws IOException {
	Properties properties = new Properties();
	InputStream inputStream = PropertiesManager.class.getClassLoader().getResourceAsStream(config);
	properties.load(inputStream);
	return properties;
    }
}
