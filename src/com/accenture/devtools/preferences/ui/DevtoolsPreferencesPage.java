package com.accenture.devtools.preferences.ui;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import com.accenture.devtools.Activator;

/**
 * main preferences page
 */
public class DevtoolsPreferencesPage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	public DevtoolsPreferencesPage() {
	}

	@Override
	public void init(IWorkbench workbench) {
	    setPreferenceStore(Activator.getDefault().getPreferenceStore());
	    setDescription("Preferences");
	}

	@Override
	protected void createFieldEditors() {
		addField(new StringFieldEditor("field_one", "Field One", 30, StringFieldEditor.VALIDATE_ON_KEY_STROKE,
				getFieldEditorParent()));
		addField(new StringFieldEditor("field_two", "Field Two", 30, StringFieldEditor.VALIDATE_ON_KEY_STROKE,
				getFieldEditorParent()));
	}
}
